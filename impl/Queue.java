package impl;

//======================================================
//=============  Array Based Queue 구현   ================ 
class Queue {
	final int DEFSIZE = 100000;
	int[] array;
	int numElement;
	int front;
	int rear;
	
	Queue(){
		array = new int[DEFSIZE];
	}
	
	boolean isFull(){
		return (array.length == numElement);
	}
	
	boolean isEmpty(){
		return (numElement==0);
	}
		
	int dequeue() throws Exception{
		
		if(isEmpty())
		{
			throw new Exception("Queue is empty!");
		}
		
		else
		{
			int r_val = array[front];
			
			array[front] = 0;
		
			
			//front = (front+1) % array.length; 와 같다
			if((front+1) >= DEFSIZE)
				front = 0;
			else
				front++;
						
			numElement--;
			
			return r_val;
		}
	}
	
	void enqueue(int item) throws Exception{
		
		if(isFull()){
			throw new Exception("Queue is full!");
		}
		else{
			
			array[rear] = item;
			
			//rear = (rear+1) % array.length; 와 같다
			if(rear >= DEFSIZE-1)
				rear = 0;
			else
				rear++;
			
			numElement++;
		}
	}
}