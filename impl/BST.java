package impl;

/*
 * BinarySearchTree Test Code
 */
/*BST bst = new BST();

bst.insert(5);
bst.insert(9);
bst.insert(3);
bst.insert(7);
bst.insert(4);

bst.postOrder(bst.root);
bst.inOrder(bst.root);
bst.preOrder(bst.root);*/

//================================================================================
//=========================  Binary Search Tree ����   =============================
class BSTNode{
	
	int element;
	private BSTNode leftChild;
	private BSTNode rightChild;
	
	public BSTNode(int element){
		this.element = element;
		setLeftChild(null);
		setRightChild(null);
	}

	public int getElement(){
		return element;
	}
	
	public BSTNode getLeftChild() {
		return leftChild;
	}

	public void setLeftChild(BSTNode leftChild) {
		this.leftChild = leftChild;
	}

	public BSTNode getRightChild() {
		return rightChild;
	}

	public void setRightChild(BSTNode rightChild) {
		this.rightChild = rightChild;
	}
	
}


public class BST {
	
	BSTNode root = null;
	
	public void insert(int element){
		BSTNode newNode = new BSTNode(element);
		BSTNode focusNode = root;
		
		if(root == null){
			root = newNode;
		}
		
		else{
			while(focusNode!=null){
				switch(nextNode(focusNode, element)){
				case -1 :
					
					if(focusNode.getLeftChild() == null){
						focusNode.setLeftChild(newNode);
						focusNode = null;
					}
					else{
						focusNode = focusNode.getLeftChild();
					}
					
					break;
					
				case 0 :
					focusNode = null;
					break;
					
				case 1 :
					
					if(focusNode.getRightChild() == null){
						focusNode.setRightChild(newNode);
						focusNode = null;
					}
					else{
						focusNode = focusNode.getRightChild();
					}
					
					break;
				}//switch
			}//while
		}//else
	}
	
	public boolean search(int item){
		
		BSTNode focusNode = root;
		
		while(focusNode != null){
			switch(nextNode(focusNode, item)){
			case -1:
				if(focusNode.getLeftChild() == null){
					return false;
				}
				else{
					focusNode = focusNode.getLeftChild();
				}
				
				break;
			case 0:
				return true;
			case 1:
				if(focusNode.getRightChild() == null){
					return false;
				}
				else{
					focusNode = focusNode.getRightChild();
				}
				
				break;
				
			}
		}
		return false;
	}
	
	int nextNode(BSTNode focusNode, int element){
		
		if(focusNode.getElement() > element){
			return -1;
		}
		else if(focusNode.getElement() < element){
			return 1;
		}
		else{
			return 0;
		}
	}
	
	public void preOrder(BSTNode node){
		BSTNode focusNode = node;
		
		if(focusNode != null){
			System.out.println(focusNode.getElement());
			preOrder(focusNode.getLeftChild());
			preOrder(focusNode.getRightChild());
		}
		
	}
	
	public void inOrder(BSTNode node){
		if(node != null){
			inOrder(node.getLeftChild());
			System.out.println(node.getElement());
			inOrder(node.getRightChild());
		}
	}
	
	public void postOrder(BSTNode node){
		if(node != null){
			postOrder(node.getLeftChild());
			postOrder(node.getRightChild());
			System.out.println(node.getElement());
		}
	}
}