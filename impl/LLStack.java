package impl;


/*
 * LinkedList Based Stack 시험 코드 
 *

LLStack stack = new LLStack();

for(int i=0; i<100000; i++){
	stack.push(""+i);			
}

if(stack.search("100001"))
	System.out.println("find!");
else
	System.out.println("Can not find!");

try{
	for(int i = 0 ; i<100000; i++){
		System.out.println(stack.pop());			
	}
}catch (Exception e) {
	e.printStackTrace();
}*/

//================================================================================
//=======================  LinkedList Based Stack 구현   ===========================
class LLStack {
	
	protected Node top;
	
	public LLStack(){
		top = null;
	}
	
	void push(String str){
		Node node = new Node(str);
		node.setLink(top);
		top = node;		
	}
	
	String pop() throws Exception{
		
		if(isEmpty()){
			throw new Exception("stack is empty!");
		}
		else{
			String r_val = (String)top.getInfo();
			top = top.getLink();
			return r_val;
		}
	}
	
	boolean search(String str){
		
		while(top!=null){
			
			if(str.equals(top.getInfo())){
				return true;
			}
			
			top = top.getLink();
			
		}
		
		return false;
	}
	
	boolean isEmpty(){
		return top == null;
	}
}

class Node {
	
	private Node link;
	private Object info;
	
	public Node(){}
	
	public Node(Object info){
		this.info = info;
		this.link = null;
	}
	
	public Node getLink() {
		return link;
	}
	public void setLink(Node link) {
		this.link = link;
	}
	public Object getInfo() {
		return info;
	}
	public void setInfo(Object info) {
		this.info = info;
	}
	
}
