package queue;

import java.util.*;

public class HistoryImpl {

	final int MAX_SIZE = 5;
	
	Queue q = new LinkedList();
	
	void save(String input){
		if(q.size() >= MAX_SIZE){
			q.poll();
		}
		q.offer(input);
	}
	
	void printCommnad(String command){
		System.out.println(command);
	}
	
	void history(Queue queue){
		int i =0;
		ListIterator it = ((LinkedList)(queue)).listIterator();
		while(it.hasNext()){
			System.out.println(++i + ". " + it.next());
		}
	}
	
	public static void main(String[] args) {
		
		HistoryImpl hi = new HistoryImpl();
		
		while(true){
			System.out.println(">>");
			
			Scanner s = new Scanner(System.in);
			String input = s.nextLine().trim();
			
			if(input.equals("")){
				continue;
			}
			if(input.equals("q")){
				System.exit(0);
			}
			if(input.equalsIgnoreCase("history")){
				hi.save(input);
				hi.history(hi.q);
			}
			else{
				hi.save(input);
				//System.out.println(input);				
			}
		}

	}

}
