package tree;

public class TreeNode {
	TreeNode left;
	Object element;
	TreeNode right;
	
	public TreeNode(Object element){
		this.element = element;
	}
	
	public void setLeft(Object left){
		this.left.element = left;
	}
	
	public void setRight(Object right){
		this.right.element = right;
	}
	
	public TreeNode getLeft(){
		return this;
	}
}
