package calc;

/*
 * This program is made by 'Suk-Young' to know when is the love anniversary.
 * Also I dedicate this program to my honey 'Su-Eun'.
 * 
 * If anyone wants to use this program, I'll permit without any message.
 * but you should make clear that I made this program.
 * If you find error or any exception, please send me a mail to below address.
 * 
 * e-mail : nscorpio16@gmail.com
 * 
 * Thank you for reading my note. Have a good day! :D
 * 2010-03-26    01:33 AM 
*/

import java.util.*;

public class CalcDDay {

	/**
	 * @param args
	 */
	static Calendar meet = Calendar.getInstance();
	static Calendar now = Calendar.getInstance();
	static Calendar anni = Calendar.getInstance();
	final static String[] DAY_OF_WEEK = {"", "일", "월", "화", "수", "목", "금", "토"};
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub		
		meet.set(2009, 6, 5);
		anni.set(2009, 6, 5);
		
		System.out.println("만난 날 : "+toString(meet)+DAY_OF_WEEK[meet.get(Calendar.DAY_OF_WEEK)]+"요일.");
		System.out.println("오늘 : "+toString(now)+DAY_OF_WEEK[now.get(Calendar.DAY_OF_WEEK)]+"요일.");
		
		long difference = (now.getTimeInMillis()-meet.getTimeInMillis())/1000;
		
		System.out.println("오늘은 만난지 "+(difference/(24*60*60)+1)+"일째 되는 날.");
		System.out.println();
		
		while(true){
			System.out.print("알고 싶은 날을 입력해 주세요(종료:Q) : ");
			Scanner s = new Scanner(System.in);
			String input = s.nextLine();
			if(input.equalsIgnoreCase("q")){
				System.out.println("오래오래 사랑하자^^ 종료되었습니다.");
				System.exit(0);
			}
			dDay(Integer.parseInt(input));			
		}
	}

	public static String toString(Calendar date) {
		// TODO Auto-generated method stub
		return date.get(Calendar.YEAR)+"년 "+(date.get(Calendar.MONTH)+1)+"월 "+date.get(Calendar.DATE)+"일 ";
	}
	
	static void dDay(int anniversary){
		anni.add(Calendar.DATE, anniversary-1);
		System.out.println("만난지 "+anniversary+"일 째 되는 날 : "+toString(anni)+"."+DAY_OF_WEEK[anni.get(Calendar.DAY_OF_WEEK)]+"요일.");
		System.out.println();
		anni.set(2009, 6, 5);
	}
}
