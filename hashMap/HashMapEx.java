package hashMap;

import java.util.*;

public class HashMapEx {

	private String inputedString(){
		Scanner s = new Scanner(System.in);
		String input = s.nextLine().trim();
		return input;
	}
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		HashMapEx h = new HashMapEx();
		HashMap map = new HashMap();
		
		for(int i=0; i<=10; i++){
			map.put(i, i*10);			
		}
		
		Set set = map.entrySet();
		Iterator iter = set.iterator();
		
		while(iter.hasNext()){
			Map.Entry e = (Map.Entry)iter.next();
			System.out.println(e.getKey()+"    "+e.getValue());
		}
	}
}
