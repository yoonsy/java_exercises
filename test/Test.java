package test;

import java.util.*;
import impl.*;

public class Test {

	public static void main(String[] args) {
		BST bst = new BST();

		bst.insert(5);
		bst.insert(9);
		bst.insert(3);
		bst.insert(7);
		bst.insert(4);
		bst.insert(1);
		
		if(bst.search(6))
			System.out.println("find");
		else
			System.out.println("failed");
	}

}
